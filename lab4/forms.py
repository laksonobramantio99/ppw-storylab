from django import forms
from .models import MySchedule
from django.forms import ModelForm

class ScheduleForm(forms.ModelForm):
    
    date = forms.DateField()
    time = forms.TimeField()
    category = forms.CharField(max_length=40)
    title = forms.CharField(max_length=40)
    location = forms.CharField(max_length=40)

    class Meta:
        model = MySchedule
        fields = ('date','time','category','title','location')
