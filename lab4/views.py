from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import MySchedule
from . import forms

# Create your views here.

def home_view(request):
    return render(request, "index.html")

def findme_view(request):
    return render(request, "findme.html")

def guest_view(request):
    return render(request, "guest.html")

def addschedule_view(request):
    return render(request, "add_schedule.html")

def addschedule_view(request):
    form = forms.ScheduleForm(request.POST)

    if form.is_valid():
        form.save()
        return redirect('schedule')
    else:
        form = forms.ScheduleForm()

    return render(request, 'add_schedule.html', {'form':form})

def schedule_view(request):
    jadwal_list = MySchedule.objects.order_by('date')
    jadwal_dict = {'jadwal':jadwal_list}
    return render(request, 'schedule.html', context=jadwal_dict)

def deleteAll_view(request):
    MySchedule.objects.all().delete()
    response = {}
    return render(request, 'schedule.html', response)