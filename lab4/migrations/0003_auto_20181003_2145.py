# Generated by Django 2.1.1 on 2018-10-03 14:45

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('lab4', '0002_auto_20181003_2142'),
    ]

    operations = [
        migrations.AddField(
            model_name='myschedule',
            name='time',
            field=models.TimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='myschedule',
            name='category',
            field=models.CharField(max_length=40),
        ),
        migrations.AlterField(
            model_name='myschedule',
            name='location',
            field=models.CharField(max_length=40),
        ),
        migrations.AlterField(
            model_name='myschedule',
            name='title',
            field=models.CharField(max_length=40),
        ),
    ]
