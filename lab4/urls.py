from django.urls import path
from .views import *

urlpatterns = [
    path('', home_view, name='home'),
    path('findme/', findme_view, name='findme'),
    path('guest/', guest_view, name='guest'),
    path('addschedule/', addschedule_view, name='addschedule'),
    path('schedule/', schedule_view, name='schedule'),
    path('schedule-wiped/', deleteAll_view, name='deleteAll'),
]

